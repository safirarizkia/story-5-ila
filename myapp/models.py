from django.db import models

# Create your models here.
class Todo(models.Model):
    matkul = models.CharField(max_length=50)
    todo = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(null=True, max_length=100)
    category = models.CharField(null=True, max_length=30)