from django.shortcuts import render
from .models import Todo
from .form import Add_ToDoList
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime
from django.shortcuts import get_object_or_404

response = {}
# Create your views here.
def index(request):
    return render(request, 'index.html',)
def about(request):
    return render(request, 'aboutme.html')
def contacts(request):
    return render(request, 'contacts.html')

def todo(request):
    toDoList = Todo.objects.all().values()
    response['toDoList'] = convert_quaryset_into_json(toDoList)
    response['form'] = Add_ToDoList
    return render(request, 'schedule.html', response)

def send(request):
    form = Add_ToDoList(request.POST)

    if request.method == 'POST' and form.is_valid():
        print(request.POST)
        date_converted = datetime.strptime(request.POST['date'], '%Y-%m-%d')
        time_converted = datetime.strptime(request.POST['time'], '%H:%M')
        log = Todo(matkul = request.POST['matkul'], todo = request.POST['todo'], date = date_converted, time = time_converted, place = request.POST['place'], category = request.POST['category'])
        log.save()

    return HttpResponseRedirect('/schedule/')

def convert_quaryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def deleteSched(request, id=None):
    toDoObject = get_object_or_404(Todo, pk=id)
    toDoObject.delete()
    return HttpResponseRedirect('/schedule/')



