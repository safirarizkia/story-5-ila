from django.urls import path
from . import views

app_name = 'myapp'

urlpatterns = [
    path('', views.index, name='index'),
    path('aboutme/', views.about, name='about'),
    path('contacts/', views.contacts, name='contacts'),
    path('schedule/', views.todo, name='schedule'),
    path('send/', views.send, name='send'),
    path('deleteSchedule/<id>',views.deleteSched, name='deleteSched'),
]