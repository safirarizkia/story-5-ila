from django import forms

class Add_ToDoList(forms.Form):
    attribute = {
        'class' : 'form-control'
    }

    matkul = forms.CharField(max_length=50, label='Mata Kuliah/Event', required=True, widget=forms.TextInput(attrs=attribute))
    todo = forms.CharField(max_length=200, label='To-Do', required=True, widget=forms.TextInput(attrs=attribute))
    date = forms.DateField(label='Date (format: YYYY-MM-DD)', required=True, widget=forms.DateInput(attrs=attribute))
    time = forms.TimeField(label='Time (format: HH:MM)', required=True, widget=forms.TimeInput(attrs=attribute))
    place = forms.CharField(max_length=100, label='Location', required=True, widget=forms.TextInput(attrs=attribute))
    category = forms.CharField(max_length=30, label='Category', required=True, widget=forms.TextInput(attrs=attribute))